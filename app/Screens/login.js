/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
import {Header,Footer,Login} from '../common/Layout';
import {styles} from '../common/Css';

var {
    View
    } = React;



module.exports = React.createClass({

    getInitialState: function() {

        return {
            title:'登录'
        }
    },


    render: function() {
        return (
            <View>
                <Header title={this.state.title} {...this.props} />

                <View style={[styles.content]}>
                    <Login {...this.props}  parent={this}/>
                </View>
                <Footer navigator={this.props.navigator} />

            </View>
        );
    },
});