/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
import {Header,Footer,Layout} from '../common/Layout';
import ListUsers from '../component/ListUsers';
import ListBox from '../component/ListBox';

import {styles} from '../common/Css';
import Config from '../common/Config';

var {
    View,
    TextInput
    } = React;



module.exports = React.createClass({

    getInitialState: function() {

        return {
            searchText:'',
            url:''

        }
    },
    _handleSearch:function(){
        if(!this.state.searchText)
            return;
        this.setState({
            url:''
        });
        var data = {q:Config.q+' '+this.state.searchText}
        var url = Config.host+'?r=Youku/GetSokuList&data='+JSON.stringify(data);
        this.setState({
            url:url
        });
        console.log(this.state.url);
    },

    render: function() {

        return (
            <View>
                <Header {...this.props} title="搜索"/>

                <View style={[styles.content]}>
                        <View style={styles.inputContainer}>
                            <TextInput
                                autoCapitalize="none"
                                autoCorrect={false}
                                autoFocus={true}
                                clearButtonMode="always"
                                returnKeyType={'search'}
                                placeholder="请输入关键词"
                                style={styles.input}
                                onChangeText={(text) => this.setState({searchText: text})}
                                onSubmitEditing={this._handleSearch}
                            />
                        </View>
                    {this.state.url?
                        <ListBox  {...this.props} url={this.state.url}/>
                        :null}
                </View>

                <Footer navigator={this.props.navigator} active="Search"/>

            </View>
        );
    },
});